const express = require('express');
const session = require('express-session')
const https = require( 'https' );
const http = require( 'http');
const mysql = require('./config/db').pool;
const bodyParser = require('body-parser');
const socket = require('socket.io');
const jwt = require('jsonwebtoken');

let app = express()
const server = http.createServer( app );
const io = socket.listen( server );
console.log("Server Started");

// ========== Get all information from table workshop
const getRoomSql = "select * from workshop WHERE (owner_id = ? OR worker_id = ?) AND id= ?";
const getRoom = (workshop_id, user_id, callback) => {
    mysql.query(getRoomSql, [user_id, user_id, workshop_id])
         .then(result => callback(null, result))
         .catch(err => callback(err, null))
}

//=========== Get type use to distinguish between of rooms
const getTypeSql = "SELECT type FROM message WHERE (sender_id = ? OR receiver_id = ?) AND (workshop_id = ? OR bid_id = ?)"
const getType = (data) =>{
    return new Promise((resolve, reject) => {
        mysql.query(getTypeSql, [data.userId, data.user_id, data.workshop_id,data.bid_id])
             .then(result => resolve(result[0].type))
             .catch(err => reject(err))
    })
}

//============ History of chat
const showHistoryChat = (results) => {
    for(var i = 0; i < results.length; i++){
        console.log(results[i].sender_id+" send "+results[i].content+ " to " +results[i].receiver_id)
    }
}

const getHistoryChatSql = "SELECT * FROM message WHERE (sender_id = ? OR receiver_id = ? ) AND (workshop_id = ? AND type = ?)";
const getHistoryChat = (user_id,room, type, callback) => {
    mysql.query(getHistoryChatSql, [user_id, user_id, room, type])
         .then(results => {
            callback(null, results)
         })
         .catch(err => callback(err, null))
}

app.get('/show_message',(req, res) => {
    res.send(tempArrs)
})

//============ Save message when new message is comming
const saveMessageSql = 'INSERT INTO message (bid_id, sender_id, content, created_at, receiver_id, status, is_notified, updated_at, workshop_id, type) VALUES (?,?,?,?,?,?,?,?,?,?)';
const saveMessage = (data, callback) => {
    var tmp_data = data;
    // Work message
    if(tmp_data.type === 1) {
        mysql.query(saveMessageSql, [tmp_data.bid_id, 
            tmp_data.sender_id,
            tmp_data.content,
            tmp_data.created_at, 
            tmp_data.receiver_id,
            tmp_data.status,
            tmp_data.is_notified,
            tmp_data.updated_at,
            null,
            tmp_data.type]).then(result => callback(null, result)).catch(err => callback(err,null));
    // Advise message
    } else if(tmp_data.type === 2){
        mysql.query(saveMessageSql, [null, 
            tmp_data.sender_id,
            tmp_data.content,
            tmp_data.created_at, 
            tmp_data.receiver_id,
            tmp_data.status,
            tmp_data.is_notified,
            tmp_data.updated_at,
            tmp_data.workshop_id,
            tmp_data.type]).then(result => callback(null, result)).catch(err => callback(err,null));
    }
    
}

app.get('/', function(req, res) {
    res.send(req.session.token)
})

io.sockets.on( 'connection', function( client ) {
    console.log( "New client !" );

    // authentication event
    client.on( 'authentication', function( data ) {
        jwt.verify(data.token, 'kjakansckasjhuih31238dmapsno12', function(err, decoded) {
            if(err){
                console.log("DISCONNECTED")
                client.emit("error_authentication",{error: 100})
                client.disconnect()
            }
        });
    });

    //Join event
    client.on('join', (data) => { //----------data inlcude workshop_id, user_id
        getRoom(data.workshop_id, data.user_id, (err, result) => { // ----- getRoom use get all data from table workshop to agree with data
            if(err) {
                //console.log(err);
                client.emit("error_join", {error: 101})
            }
            else {
                // Join client to specify chatroom
                client.join(data.workshop_id)
                io.sockets.to(data.workshop_id).emit("join_success", { room: data.workshop_id })
            }
        })
    })

    // Chat event
    client.on("chat",(data) => {
        saveMessage(data, (err, result)=> {
            if(err) {
                console.log(err)
                // Only send error back to sender
                client.to(data.workshop_id).emit("chat_error", {error: 102})
            }
            else {
                console.log("Chat success " + data.workshop_id)
                io.sockets.to(data.workshop_id).emit("chat_success", { data })
            }
        })
    })
    
    // Chat event old
    client.on("chat_old_ws",(data) => {
        console.log("Chat success " + data.content)
        client.broadcast.to(data.workshop_id).emit("chat_success", { data })
    })

    // Refresh event
    client.on("refresh_page",(data) => {
        if(data.workshop_id)
            // Event for partners only
            client.broadcast.to(data.workshop_id).emit('refresh_page_return');
    })
});


let port = 8181;
server.listen(port,() => {
    console.log("Your server listen at port " + port)
})