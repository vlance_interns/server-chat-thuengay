var mysql = require('promise-mysql');

var pool  = mysql.createPool({
    host     : 'localhost',
    user     : 'root',
    password : '',
    database : 'thuengay_dev'
});

exports.pool = pool;